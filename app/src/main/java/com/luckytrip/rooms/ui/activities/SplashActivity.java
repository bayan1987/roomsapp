package com.luckytrip.rooms.ui.activities;

import android.os.Bundle;
import android.os.Handler;
import android.view.WindowManager;

import com.luckytrip.rooms.R;
import com.luckytrip.rooms.helpers.ActivityHelper;
import com.luckytrip.rooms.helpers.Constants;

public class SplashActivity extends BaseActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);


        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                    ActivityHelper.goToActivity(SplashActivity.this,MainActivity.class,true);

            }
        }, Constants.SPLASH_TIMEOUT);
    }
}
