package com.luckytrip.rooms.ui.fragments;

import android.net.Uri;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.databinding.DataBindingUtil;

import com.luckytrip.rooms.R;
import com.luckytrip.rooms.databinding.FragmentRoomInfoBinding;
import com.luckytrip.rooms.helpers.Constants;
import com.luckytrip.rooms.models.RoomModel;

public class RoomInfoFragment extends BaseFragment{

    private FragmentRoomInfoBinding binding;
    private RoomModel selectedRoom;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_room_info, container, false);
        selectedRoom = getArguments().getParcelable(Constants.KEY_SELECTED_ROOM);
        return binding.getRoot();
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        initUI();
    }

    private void initUI() {
        binding.ivIcon.setImageURI(Uri.parse(getArguments().getString(Constants.KEY_SELECTED_PHOTO)));
        binding.tvRoomName.setText(selectedRoom.getName());
    }
}
