package com.luckytrip.rooms.ui.adapters;

import android.content.Context;
import android.net.Uri;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.luckytrip.rooms.R;
import com.luckytrip.rooms.databinding.AdapterRoomBinding;
import com.luckytrip.rooms.models.RoomModel;
import com.luckytrip.rooms.ui.custom.chipcloud.ChipCloud;

import java.lang.ref.WeakReference;
import java.util.ArrayList;

public class RoomsAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private ArrayList<RoomModel> data;
    private WeakReference<Context> ctx;
    private OnItemClickListener mItemClickListener;


    public RoomsAdapter(Context ctx , ArrayList<RoomModel> arrayList) {
        this.data = arrayList;
        this.ctx = new WeakReference<>(ctx);
    }



    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        AdapterRoomBinding binding = AdapterRoomBinding.inflate(LayoutInflater.from(parent.getContext()), parent, false);
        return new ItemViewHolder(binding);
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {

        ItemViewHolder iHolder = (ItemViewHolder)holder;
        iHolder.binding.tvName.setText(data.get(position).getName());
        iHolder.binding.tvDesc.setText(data.get(position).getRoomDescription());
        iHolder.binding.tvPrice.setText(data.get(position).getPrice().getCurrency() +" "+data.get(position).getPrice().getPriceValue());

        if(!data.get(position).getPhotos().isEmpty()){
            iHolder.binding.ivIcon.setImageURI(Uri.parse(data.get(position).getPhotos().get(0)));
        }

        iHolder.binding.tvMaxGuests.setText(ctx.get().getString(R.string.max_guests)+": "+data.get(position).getMaxOccupancy());

        String values [] = new String[data.get(position).getBedConfigurations().size()];
        for (int i = 0; i <data.get(position).getBedConfigurations().size() ; i++) {
            values[i]= data.get(position).getBedConfigurations().get(i).getCount()+" "+data.get(position).getBedConfigurations().get(i).getName();
        }
        new ChipCloud.Configure()
                .chipCloud(iHolder.binding.chipCloud)
                .labels(values)
                .build();

    }

    @Override
    public int getItemCount() {
        return data.size();
    }

    class ItemViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        private AdapterRoomBinding binding;


        public ItemViewHolder(AdapterRoomBinding binding) {
            super(binding.getRoot());
            this.binding = binding;
            binding.getRoot().setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {
            if (mItemClickListener != null) {
                mItemClickListener.onItemClick(itemView, getAdapterPosition());
            }

        }
    }

    public void setItemClickListener(OnItemClickListener mItemClickListener) {
        this.mItemClickListener = mItemClickListener;
    }
}
