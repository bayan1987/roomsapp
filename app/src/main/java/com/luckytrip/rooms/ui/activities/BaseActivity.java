package com.luckytrip.rooms.ui.activities;

import android.os.Bundle;
import android.util.Log;
import androidx.appcompat.app.AppCompatActivity;

public  class BaseActivity extends AppCompatActivity {


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Override
    protected void onPause() {
        super.onPause();
        Log.w(this.getClass().getSimpleName(), "Base -- onPause");
        overridePendingTransition(0, 0);
    }
}
