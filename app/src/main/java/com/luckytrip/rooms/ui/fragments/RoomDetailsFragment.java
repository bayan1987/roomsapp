package com.luckytrip.rooms.ui.fragments;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProviders;

import com.luckytrip.rooms.R;
import com.luckytrip.rooms.databinding.FragmentRoomDetailsBinding;
import com.luckytrip.rooms.databinding.FragmentRoomsBinding;
import com.luckytrip.rooms.helpers.Constants;
import com.luckytrip.rooms.models.RoomModel;
import com.luckytrip.rooms.ui.adapters.PagerAdapter;
import com.luckytrip.rooms.viewmodels.RoomVM;

public class RoomDetailsFragment extends BaseFragment {

    private FragmentRoomDetailsBinding binding;
    private RoomModel selectedRoom;
    private PagerAdapter adapter;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_room_details, container, false);
        selectedRoom = getArguments().getParcelable(Constants.KEY_SELECTED_ROOM);
        return binding.getRoot();
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        initUI();
    }

    private void initUI() {
        if(!selectedRoom.getPhotos().isEmpty()){
            adapter = new PagerAdapter(getActivity().getSupportFragmentManager());
            for (int i = 0; i <selectedRoom.getPhotos().size() ; i++) {
                Bundle bundle = new Bundle();
                bundle.putParcelable(Constants.KEY_SELECTED_ROOM,selectedRoom);
                bundle.putString(Constants.KEY_SELECTED_PHOTO, selectedRoom.getPhotos().get(i));
                RoomInfoFragment fragment = new RoomInfoFragment();
                fragment.setArguments(bundle);
                adapter.addFragment(fragment);

            }

            binding.pager.setAdapter(adapter);
            binding.dotsIndicator.setViewPager(binding.pager);
        }else {

        }

        binding.tvPrice.setText(selectedRoom.getPrice().getCurrency()+" "+selectedRoom.getPrice().getPriceValue());

        if(selectedRoom.getRoomDescription().isEmpty()){
            binding.lblDesc.setVisibility(View.INVISIBLE);
        }else {
            binding.tvDesc.setText(selectedRoom.getRoomDescription());
        }


    }
}
