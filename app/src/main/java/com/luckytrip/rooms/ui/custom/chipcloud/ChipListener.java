package com.luckytrip.rooms.ui.custom.chipcloud;

public interface ChipListener {
    void chipSelected(int index);

    void chipDeselected(int index);
}
