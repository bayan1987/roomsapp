package com.luckytrip.rooms.ui.fragments;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.FragmentManager;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.luckytrip.rooms.R;
import com.luckytrip.rooms.data.DataManager;
import com.luckytrip.rooms.databinding.FragmentRoomsBinding;
import com.luckytrip.rooms.helpers.Constants;
import com.luckytrip.rooms.helpers.NetworkHelper;
import com.luckytrip.rooms.models.RoomModel;
import com.luckytrip.rooms.models.RoomResponse;
import com.luckytrip.rooms.ui.adapters.OnItemClickListener;
import com.luckytrip.rooms.ui.adapters.RoomsAdapter;
import com.luckytrip.rooms.viewmodels.ErrorModel;
import com.luckytrip.rooms.viewmodels.RoomVM;
import com.luckytrip.rooms.viewmodels.UIState;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;

public class RoomsListFragment extends BaseFragment implements OnItemClickListener {

    private FragmentRoomsBinding binding;
    private RoomVM viewModel;

    private RoomsAdapter adapter;
    private ArrayList<RoomModel> data = new ArrayList<>();

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_rooms, container, false);

        viewModel = ViewModelProviders.of(this).get(RoomVM.class);

        binding.lytLoading.setUiState(viewModel.getUiState());
        binding.noInternet.setNoInternetConnection(viewModel.getUiState());
        binding.serverErrorLayout.setUiStateServerError(viewModel.getUiState());
        binding.serverErrorLayout.setMessage(viewModel.getTextMessage());
        binding.noRooms.setUiStateNoRooms(viewModel.getUiState());
        binding.setLifecycleOwner(this);
        binding.setViewModel(viewModel);

        return binding.getRoot();
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        initUI();
    }

    private void initUI() {

        binding.recList.setLayoutManager(new LinearLayoutManager(getActivity(), RecyclerView.VERTICAL, false));
        adapter = new RoomsAdapter(getContext(),data);
        binding.recList.setAdapter(adapter);
        adapter.setItemClickListener(this);

        getRooms();

    }

    private void getRooms(){
        if(NetworkHelper.isNetworkAvailable()){
            viewModel.setUiState(UIState.LOADING);
            data.clear();
            adapter.notifyDataSetChanged();
            viewModel.getRooms().observe(getViewLifecycleOwner(), new Observer<RoomResponse>() {
                @Override
                public void onChanged(RoomResponse roomResponse) {

                    if(roomResponse.isSuccess()){
                        if(!roomResponse.getRooms().isEmpty()){

                            data.addAll(roomResponse.getRooms());
                            Collections.sort(data, new Comparator<RoomModel>(){
                                public int compare(RoomModel obj1, RoomModel obj2) {
                                    // ## Ascending order
                                    return Integer.valueOf(obj1.getMaxOccupancy()).compareTo(Integer.valueOf(obj2.getMaxOccupancy()));
                                }
                            });


                            adapter.notifyDataSetChanged();
                        }
                        viewModel.setUiState(UIState.SUCCESS);
                    }else {
                        viewModel.setUiState(UIState.SERVER_ERROR);
                        ErrorModel errorModel = new ErrorModel();
                        errorModel.setErrorMessage(roomResponse.getErrorTypes());
                        viewModel.setTextMessage(errorModel);
                    }
                }
            });
        }else {
            viewModel.setUiState(UIState.NETWORK_ERROR);
        }
    }

    @Override
    public void onItemClick(View view, int position) {

        goToRoomsDetailsFragment(data.get(position));
    }

    public void goToRoomsDetailsFragment(RoomModel selectedRoom) {

        DataManager.getInstance().saveSelectedRoom(selectedRoom);

        Bundle bundle = new Bundle();
        bundle.putParcelable(Constants.KEY_SELECTED_ROOM , selectedRoom);

        RoomDetailsFragment fragment = new RoomDetailsFragment();
        fragment.setArguments(bundle);
//        getActivity().getSupportFragmentManager().popBackStack(null, FragmentManager.POP_BACK_STACK_INCLUSIVE);
        getActivity().getSupportFragmentManager().beginTransaction()
                .addToBackStack("")
                .replace(R.id.frame_layout, fragment).commit();

    }
}
