package com.luckytrip.rooms.ui.adapters;

import android.view.View;

/**
 * Created by bayan on 2/19/18.
 */

public interface OnItemClickListener {
    public void onItemClick(View view, int position);
}
