package com.luckytrip.rooms.ui.fragments;

import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.FragmentManager;

import com.luckytrip.rooms.R;
import com.luckytrip.rooms.data.DataManager;
import com.luckytrip.rooms.databinding.FragmentHomeBinding;
import com.luckytrip.rooms.helpers.LoggerHelper;
import com.luckytrip.rooms.models.RoomModel;

import eightbitlab.com.blurview.RenderScriptBlur;



public class HomeFragment extends BaseFragment {

    private FragmentHomeBinding binding;
    private int mInterval = 5000;
    private Handler mHandler;
    private int index = 0;


    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_home, container, false);
        return binding.getRoot();
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        initUI();
    }

    private void initUI() {
        mHandler = new Handler();

        float radius = 10f;

        View decorView = getActivity().getWindow().getDecorView();
        ViewGroup rootView = (ViewGroup) decorView.findViewById(android.R.id.content);
        Drawable windowBackground = decorView.getBackground();

        binding.blurView.setupWith(rootView)
                .setFrameClearDrawable(windowBackground)
                .setBlurAlgorithm(new RenderScriptBlur(getContext()))
                .setBlurRadius(radius)
                .setHasFixedTransformationMatrix(true);

        if(DataManager.getInstance().getSelectedRoom() == null){
            binding.cardView.setVisibility(View.INVISIBLE);
        }else {

            RoomModel selectedRoom = DataManager.getInstance().getSelectedRoom();
            binding.tvName.setText(selectedRoom.getName());
            binding.tvPrice.setText(selectedRoom.getPrice().getCurrency()+" "+selectedRoom.getPrice().getPriceValue());
            binding.tvDesc.setText(selectedRoom.getRoomDescription());

            if(!selectedRoom.getPhotos().isEmpty()){
                startRepeatingTask();
            }
        }

        binding.btnBook.setOnClickListener((view)-> {
            goToRoomsFragment();
        });
    }

    public void goToRoomsFragment() {
        getActivity().getSupportFragmentManager().beginTransaction()
                .addToBackStack("")
                .replace(R.id.frame_layout, new RoomsListFragment()).commit();

    }

    @Override
    public void onStop() {
        super.onStop();
        stopRepeatingTask();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();

    }

    Runnable mStatusChecker = new Runnable() {
        @Override
        public void run() {
            try {
                if(index< DataManager.getInstance().getSelectedRoom().getPhotos().size()){
                    binding.back.setImageURI(Uri.parse(DataManager.getInstance().getSelectedRoom().getPhotos().get(index)));
                    index++;
                    if(index == DataManager.getInstance().getSelectedRoom().getPhotos().size())
                        index = 0;
                }else {
                    index = 0;
                }

            } finally {
                mHandler.postDelayed(mStatusChecker, mInterval);
            }
        }
    };

    void startRepeatingTask() {
        mStatusChecker.run();
    }

    void stopRepeatingTask() {
        mHandler.removeCallbacks(mStatusChecker);
    }
}
