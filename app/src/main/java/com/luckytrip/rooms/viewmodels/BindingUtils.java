package com.luckytrip.rooms.viewmodels;

import android.text.TextUtils;
import android.view.View;

import androidx.databinding.BindingAdapter;

import com.luckytrip.rooms.helpers.LoggerHelper;

public final class BindingUtils {



    @BindingAdapter("visibleIf")
    public static void changeVisibility(View view, boolean visibleIf){
        LoggerHelper.error("visibleIf: "+visibleIf);
        if(visibleIf){
            view.setVisibility(View.VISIBLE);
        } else {
            view.setVisibility(View.GONE);
        }
    }
}
