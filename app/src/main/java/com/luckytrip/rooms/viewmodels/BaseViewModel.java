package com.luckytrip.rooms.viewmodels;

import android.app.Application;

import androidx.annotation.NonNull;
import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;

import com.luckytrip.rooms.helpers.LoggerHelper;

public class BaseViewModel extends AndroidViewModel {



    public final MutableLiveData<UIState> uiState = new MutableLiveData<>();
    public final MutableLiveData<ErrorModel> textMessage = new MutableLiveData<>();

    public BaseViewModel(@NonNull Application application) {
        super(application);
    }


    public LiveData<UIState> getUiState() {
        return uiState;
    }

    public void setUiState(UIState newState){
        uiState.setValue(newState);
    }


    public void setTextMessage( ErrorModel errorModel){
        LoggerHelper.error("Message....."+errorModel.getErrorMessage());
        textMessage.setValue(errorModel);
    }

    public LiveData<ErrorModel> getTextMessage(){
        return textMessage;
    }

}
