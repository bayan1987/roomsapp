package com.luckytrip.rooms.viewmodels;

import android.app.Application;

import androidx.annotation.NonNull;
import androidx.lifecycle.LiveData;

import com.luckytrip.rooms.data.remote.repos.RoomsListRepo;
import com.luckytrip.rooms.models.RoomResponse;

public class RoomVM extends BaseViewModel {

    private RoomsListRepo roomsListRepo;
    public RoomVM(@NonNull Application application) {
        super(application);
        roomsListRepo = new RoomsListRepo(application);
    }

    public LiveData<RoomResponse> getRooms(){
        return roomsListRepo.getMutableData("en");
    }
}
