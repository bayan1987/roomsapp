package com.luckytrip.rooms.helpers;

import android.util.Log;

import com.luckytrip.rooms.BuildConfig;


public class LoggerHelper {
    public static final boolean DEBUG_EANBLED = BuildConfig.DEBUG;

    public static void info(String msg){
        if(DEBUG_EANBLED){
            Log.i(Constants.TAG, msg);
        }
    }

    public static void error(String msg){
        if(DEBUG_EANBLED){
            Log.e(Constants.TAG, msg);
        }
    }

    public static void error(String key, String msg){
        if(DEBUG_EANBLED){
            Log.e(key, msg);
        }
    }

    public static void verbose(String msg){
        if(DEBUG_EANBLED){
            Log.v(Constants.TAG, msg);
        }
    }

    public static void debug(String msg){
        if(DEBUG_EANBLED){
            Log.d(Constants.TAG, msg);
        }
    }

    public static void warning(String msg){
        if(DEBUG_EANBLED){
            Log.w(Constants.TAG, msg);
        }
    }
}
