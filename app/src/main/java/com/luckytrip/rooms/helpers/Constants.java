package com.luckytrip.rooms.helpers;

public class Constants {
    public static final int SPLASH_TIMEOUT = 3000;
    public static final String TAG = "Rooms";

    public static final String  KEY_SELECTED_ROOM = "KEY_SELECTED_ROOM";
    public static final String KEY_SELECTED_PHOTO = "KEY_SELECTED_PHOTO";

}
