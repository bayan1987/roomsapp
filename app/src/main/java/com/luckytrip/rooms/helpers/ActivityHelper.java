package com.luckytrip.rooms.helpers;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;

public class ActivityHelper {

    public  static void goToActivity(Context ctx, Class<?> to , boolean isFinished) {

        Intent i = new Intent(ctx, to);
        i.addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
//        i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP );
//        i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        ctx.startActivity(i);
//        ((Activity)ctx).overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
        if(isFinished)
            ((Activity) ctx).finish();

    }

    public  static void goToActivityWithNewTask(Context ctx, Class<?> to ) {

        Intent i = new Intent(ctx, to);
        i.addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
        i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP );
        i.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);

//        i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_EXCLUDE_FROM_RECENTS);
        ctx.startActivity(i);


    }

    public  static void goToActivity(Context ctx, Class<?> to , boolean isFinished , Bundle bundle) {

        Intent i = new Intent(ctx, to);
        i.addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
//        i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP );
//        i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        i.putExtras(bundle );
        ctx.startActivity(i);
        if(isFinished)
            ((Activity) ctx).finish();

    }

    public static void startActivityForResult(Context ctx, Class<?> to , int id , Bundle bundle){
        Intent i = new Intent(ctx, to);
        i.addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
//        i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP );
        if(bundle!= null)
            i.putExtras(bundle );
        ((Activity)ctx).startActivityForResult(i, id);

    }

    public  static void goToActivityWithClearTop(Context ctx, Class<?> to , boolean isFinished) {

        Intent i = new Intent(ctx, to);
        i.addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
        i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP );
//        i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        ctx.startActivity(i);
        if(isFinished)
            ((Activity) ctx).finish();

    }

    public  static void goToActivityWithClearTop(Context ctx, Class<?> to , boolean isFinished , Bundle bundle) {

        Intent i = new Intent(ctx, to);
//        i.addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
        i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP );
        i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        i.putExtras(bundle );
        ctx.startActivity(i);
        if(isFinished)
            ((Activity) ctx).finish();

    }
}
