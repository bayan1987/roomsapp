package com.luckytrip.rooms;

import android.app.Application;
import android.os.Environment;

import com.facebook.cache.disk.DiskCacheConfig;
import com.facebook.common.internal.Supplier;
import com.facebook.common.util.ByteConstants;
import com.facebook.drawee.backends.pipeline.Fresco;
import com.facebook.imagepipeline.core.ImagePipelineConfig;

import java.io.File;

public class RoomApp extends Application {

    private static RoomApp sInstance;

    @Override
    public void onCreate() {
        super.onCreate();
        sInstance = this;
        initFresco();
    }

    public static RoomApp getInstance() {
        return sInstance;
    }

    private void initFresco(){

        Supplier<File> diskSupplier = new Supplier<File>() {
            public File get() {
                return getApplicationContext().getCacheDir();
            }
        };


        DiskCacheConfig diskCacheConfig = DiskCacheConfig.newBuilder(this)
                .setBaseDirectoryPath(new File(Environment.getExternalStorageDirectory().getAbsoluteFile(),getPackageName()))
                .setBaseDirectoryName("fresco")
                .setBaseDirectoryPathSupplier(diskSupplier)
                .setMaxCacheSize(50 * ByteConstants.MB)
                .setMaxCacheSizeOnLowDiskSpace(20 * ByteConstants.MB)
                .setMaxCacheSizeOnVeryLowDiskSpace(15 * ByteConstants.MB)
                .setVersion(1)
                .build();



        ImagePipelineConfig imagePipelineConfig = ImagePipelineConfig.newBuilder(this)
                .setMainDiskCacheConfig(diskCacheConfig)
                .setDownsampleEnabled(true)
                .build();

        Fresco.initialize(this, imagePipelineConfig);

    }

}
