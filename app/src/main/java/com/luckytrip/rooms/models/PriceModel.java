package com.luckytrip.rooms.models;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;

import lombok.Data;

@Data
public class PriceModel implements Parcelable {

    @SerializedName("currency")
    private String currency;

    @SerializedName("price_value")
    private float priceValue;

    protected PriceModel(Parcel in) {
        currency = in.readString();
        priceValue = in.readFloat();
    }

    public static final Creator<PriceModel> CREATOR = new Creator<PriceModel>() {
        @Override
        public PriceModel createFromParcel(Parcel in) {
            return new PriceModel(in);
        }

        @Override
        public PriceModel[] newArray(int size) {
            return new PriceModel[size];
        }
    };

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeString(currency);
        parcel.writeFloat(priceValue);
    }
}
