package com.luckytrip.rooms.models;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

import lombok.Data;

@Data
public class RoomResponse implements Parcelable {

    @SerializedName("rooms")
    private ArrayList<RoomModel> rooms;

    private boolean success;
    private String errorTypes;


    public RoomResponse() {
    }

    protected RoomResponse(Parcel in) {
        rooms = in.createTypedArrayList(RoomModel.CREATOR);
        success = in.readByte() != 0;
        errorTypes = in.readString();
    }

    public static final Creator<RoomResponse> CREATOR = new Creator<RoomResponse>() {
        @Override
        public RoomResponse createFromParcel(Parcel in) {
            return new RoomResponse(in);
        }

        @Override
        public RoomResponse[] newArray(int size) {
            return new RoomResponse[size];
        }
    };

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeTypedList(rooms);
        parcel.writeByte((byte) (success ? 1 : 0));
        parcel.writeString(errorTypes);
    }
}
