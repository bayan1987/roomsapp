package com.luckytrip.rooms.models;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;

import lombok.Data;

@Data
public class BedConfigModel implements Parcelable {

    @SerializedName("count")
    private int count;

    @SerializedName("name")
    private String name;

    protected BedConfigModel(Parcel in) {
        count = in.readInt();
        name = in.readString();
    }

    public static final Creator<BedConfigModel> CREATOR = new Creator<BedConfigModel>() {
        @Override
        public BedConfigModel createFromParcel(Parcel in) {
            return new BedConfigModel(in);
        }

        @Override
        public BedConfigModel[] newArray(int size) {
            return new BedConfigModel[size];
        }
    };

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeInt(count);
        parcel.writeString(name);
    }
}
