package com.luckytrip.rooms.models;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

import lombok.Data;

@Data
public class RoomModel  implements Parcelable {

    @SerializedName("room_id")
    private int roomId;

    @SerializedName("name")
    private String name;

    @SerializedName("room_description")
    private String roomDescription;

    @SerializedName("number_of_rooms_left")
    private int numberOfRoomsLeft;

    @SerializedName("max_occupancy")
    private String maxOccupancy;

    @SerializedName("price")
    private PriceModel price;

    @SerializedName("bed_configurations")
    private ArrayList<BedConfigModel> bedConfigurations;

    @SerializedName("photos")
    private ArrayList<String> photos;


    protected RoomModel(Parcel in) {
        roomId = in.readInt();
        name = in.readString();
        roomDescription = in.readString();
        numberOfRoomsLeft = in.readInt();
        maxOccupancy = in.readString();
        photos = in.createStringArrayList();
    }

    public static final Creator<RoomModel> CREATOR = new Creator<RoomModel>() {
        @Override
        public RoomModel createFromParcel(Parcel in) {
            return new RoomModel(in);
        }

        @Override
        public RoomModel[] newArray(int size) {
            return new RoomModel[size];
        }
    };

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeInt(roomId);
        parcel.writeString(name);
        parcel.writeString(roomDescription);
        parcel.writeInt(numberOfRoomsLeft);
        parcel.writeString(maxOccupancy);
        parcel.writeStringList(photos);
    }
}
