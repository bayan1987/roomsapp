package com.luckytrip.rooms.data.local.prefs;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;

import com.google.gson.Gson;
import com.luckytrip.rooms.RoomApp;

public class PreferencesHelper implements PreferencesKeys {

    private static PreferencesHelper INSTANCE;

    public static synchronized PreferencesHelper getINSTANCE(Context context) {
        if(INSTANCE==null){
            INSTANCE = new PreferencesHelper(PreferenceManager.getDefaultSharedPreferences(context));
        }
        return INSTANCE;
    }

    private SharedPreferences sharedPreferences;

    private PreferencesHelper(SharedPreferences sharedPreferences) {
        this.sharedPreferences = sharedPreferences;
    }

    public void setInt(String key, int value){
        sharedPreferences.edit().putInt(key,value).apply();
    }

    public int getInt(String key, int _default){
        return sharedPreferences.getInt(key, _default);
    }

    public void setString(String key, String value){
        sharedPreferences.edit().putString(key,value).apply();
    }

    public String getString(String key, String _default){
        return sharedPreferences.getString(key, _default);
    }


    public void setLong(String key, Long value){
        sharedPreferences.edit().putLong(key,value).apply();
    }

    public Long getLong(String key, Long _default){
        return sharedPreferences.getLong(key, _default);
    }


    public void setFloat(String key , float value){
        sharedPreferences.edit().putFloat(key,value);
    }

    public float getFloat(String key, float _default){
        return sharedPreferences.getFloat(key,_default);
    }



    public void clearPreferences(Context prefcontect, String name) {

        sharedPreferences.edit().clear().commit();
    }

    public static boolean saveObject(String prefKey, Object object) {
        SharedPreferences preferences= PreferenceManager.getDefaultSharedPreferences(RoomApp.getInstance());
        SharedPreferences.Editor editor=preferences.edit();
        try {
            Gson gson = new Gson();
            String json = gson.toJson(object);
            editor.putString(prefKey, json);
            editor.apply();
            return true;
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
    }

    public static <T> T getObject(String key, Class<T> type) {
        SharedPreferences preferences= PreferenceManager.getDefaultSharedPreferences(RoomApp.getInstance());

        String json =preferences.getString(key, "");// getSharedPreferences(context).getString(prefKey, null);
        if (json != null) {
            try {
                Gson gson = new Gson();
                T result = gson.fromJson(json, type);
                return result;
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        return null;
    }



}
