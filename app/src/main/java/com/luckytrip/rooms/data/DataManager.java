package com.luckytrip.rooms.data;

import com.luckytrip.rooms.RoomApp;
import com.luckytrip.rooms.data.local.prefs.PreferencesHelper;
import com.luckytrip.rooms.data.local.prefs.PreferencesKeys;
import com.luckytrip.rooms.models.RoomModel;

public class DataManager {

    private static DataManager sInstance;


    private DataManager() {
        // This class is not publicly instantiable
    }

    public static synchronized DataManager getInstance() {
        if (sInstance == null) {
            sInstance = new DataManager();
        }
        return sInstance;
    }


    public void saveSelectedRoom(RoomModel roomModel){
        PreferencesHelper preferencesHelper = PreferencesHelper.getINSTANCE(RoomApp.getInstance());
        preferencesHelper.saveObject(PreferencesKeys.KEY_SELECTED_ROOM,roomModel);
    }

    public RoomModel getSelectedRoom(){
        return PreferencesHelper.getINSTANCE(RoomApp.getInstance()).getObject(PreferencesKeys.KEY_SELECTED_ROOM,RoomModel.class);
    }

}
