package com.luckytrip.rooms.data.remote;


import com.luckytrip.rooms.models.RoomResponse;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Query;

public interface APIEndpoints {

    @GET("rooms")
    Call<RoomResponse> getRooms(@Query("language_code") String language);


}
