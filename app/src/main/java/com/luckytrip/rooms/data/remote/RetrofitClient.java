package com.luckytrip.rooms.data.remote;


import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.luckytrip.rooms.BuildConfig;

import org.jetbrains.annotations.NotNull;

import java.io.IOException;
import java.util.concurrent.TimeUnit;

import okhttp3.Interceptor;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;
import okhttp3.internal.platform.Platform;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

import static okhttp3.internal.platform.Platform.INFO;


public class RetrofitClient {

    private static final boolean ENABLE_LOGGING = true;
    public static String SERVER_URL = BuildConfig.BASE_URL;
    private static RetrofitClient apiManager;
    private APIEndpoints apiEndpoints;
    private static final int CONNECTION_TIME_OUT = 120000;

    private RetrofitClient() {
        Gson gson = new GsonBuilder()
                .setDateFormat("yyyy-MM-dd'T'HH:mm:ssZ")
                .setLenient()
                .create();

        HttpLoggingInterceptor interceptor = new HttpLoggingInterceptor(new HttpLoggingInterceptor.Logger() {
            @Override
            public void log(String message) {

                try {
                    Platform.get().log(message, INFO, null);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });

        if (ENABLE_LOGGING) {
            interceptor.setLevel(HttpLoggingInterceptor.Level.BODY);

        }

        OkHttpClient client = new OkHttpClient.Builder().
                readTimeout(CONNECTION_TIME_OUT, TimeUnit.SECONDS)
                .connectTimeout(CONNECTION_TIME_OUT, TimeUnit.SECONDS)
                .addInterceptor(new Interceptor() {
                    @NotNull
                    @Override
                    public Response intercept(@NotNull Chain chain) throws IOException {
                        Request original = chain.request();

                        Request request = original.newBuilder()
                                .header("Content-Type", "application/json")
                                .method(original.method(), original.body())
                                .build();

                        return chain.proceed(request);
                    }
                })
                .addInterceptor(interceptor)
                .build();


        Retrofit retrofit = new Retrofit.Builder()
                .client(client)
                .baseUrl(SERVER_URL)
                .addConverterFactory(GsonConverterFactory.create(gson))
                .build();

        apiEndpoints = retrofit.create(APIEndpoints.class);
    }

    public static RetrofitClient getInstance() {
        if (apiManager == null) {
            apiManager = new RetrofitClient();
        }

        return apiManager;
    }

    public APIEndpoints getApiService() {
        return apiEndpoints;
    }

}
