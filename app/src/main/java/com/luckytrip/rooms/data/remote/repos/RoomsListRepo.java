package com.luckytrip.rooms.data.remote.repos;

import android.app.Application;

import androidx.lifecycle.MutableLiveData;

import com.google.gson.Gson;
import com.luckytrip.rooms.data.remote.RetrofitClient;
import com.luckytrip.rooms.models.RoomResponse;

import java.io.IOException;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class RoomsListRepo {

    private Application application;

    public RoomsListRepo(Application application) {
        this.application = application;
    }


    public MutableLiveData<RoomResponse> getMutableData(String language) {

        final MutableLiveData<RoomResponse> mutableLiveData = new MutableLiveData<>();

        Call<RoomResponse> call = RetrofitClient.getInstance().getApiService().getRooms(language);
        call.enqueue(new Callback<RoomResponse>() {
            @Override
            public void onResponse(Call<RoomResponse> call, Response<RoomResponse> response) {
                if (response.isSuccessful()) {
                    RoomResponse roomResponse = response.body();
                    roomResponse.setSuccess(true);
                    mutableLiveData.setValue(roomResponse);
                } else {
                    try {
                        String json = response.errorBody().string();
                        RoomResponse responseError = new Gson().fromJson(json, RoomResponse.class);
                        responseError.setSuccess(false);
                        mutableLiveData.setValue(responseError);

                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
            }

            @Override
            public void onFailure(Call<RoomResponse> call, Throwable t) {
                RoomResponse error = new RoomResponse();
                error.setSuccess(false);
                error.setErrorTypes(t.getMessage());
                mutableLiveData.setValue(error);
            }
        });


        return mutableLiveData;
    }
    
}
